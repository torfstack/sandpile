use std::{env, collections::VecDeque};
use image::{ImageBuffer, RgbImage};

fn main() {
    let args: Vec<String> = env::args().collect();
    println!("Called with arguments {} {}", args[1], args[2]);
    if args.len() != 3 {
        println!("sandpile only supports two(2) integer argument");
        return;
    }
    let size = args[1].parse::<usize>().unwrap();
    let start = args[2].parse::<i32>().unwrap();
    if size%2 != 0 {
        println!("size parameter has to be even");
        return;
    }

    let mut initial = prepare(&size, &start);
    let finished = do_work(&mut initial);
    draw_image(finished, &size);
}

fn prepare(size: &usize, start: &i32) -> Vec<Vec<i32>> {
    let mut initial = vec![vec![0; *size]; *size];
    initial[size/2][size/2] = *start;
    initial[size/2-1][size/2] = *start;
    initial[size/2][size/2-1] = *start;
    initial[size/2-1][size/2-1] = *start;
    return initial;
}

fn do_work(matrix: &mut Vec<Vec<i32>>) -> &Vec<Vec<i32>> {
    let mut queue: VecDeque<(usize, usize)> = VecDeque::new();
    queue.push_back((matrix.len()/2, matrix.len()/2));
    queue.push_back((matrix.len()/2 - 1, matrix.len()/2));
    queue.push_back((matrix.len()/2, matrix.len()/2 - 1));
    queue.push_back((matrix.len()/2 - 1, matrix.len()/2 - 1));

    while !queue.is_empty() {
        let (x, y) = queue.pop_front().unwrap();
        let value = matrix[x][y];
        if value < 4 { continue; }
        let remainder = value%4;
        let overflow = value/4;
        modify(matrix, x, y, remainder, overflow, matrix.len(), &mut queue);
    }

    return matrix;
}

fn modify(matrix: &mut Vec<Vec<i32>>, x: usize, y: usize, remainder: i32, overflow: i32, size: usize, queue: &mut VecDeque<(usize, usize)>) {
    matrix[x][y] = remainder;
    if x > 0 { 
        let value = matrix[x-1][y];
        matrix[x-1][y] = value + overflow; 
        if value + overflow > 3 { queue.push_back((x-1, y)); }
    }
    if y > 0 { 
        let value = matrix[x][y-1];
        matrix[x][y-1] = value + overflow; 
        if value + overflow > 3 { queue.push_back((x, y-1)); }
    }
    if x+1 < size { 
        let value = matrix[x+1][y];
        matrix[x+1][y] = value + overflow; 
        if value + overflow > 3 { queue.push_back((x+1, y)); } 
    }
    if y+1 < size { 
        let value = matrix[x][y+1];
        matrix[x][y+1] = value + overflow; 
        if value + overflow > 3 { queue.push_back((x, y+1)); }
    }
}

fn draw_image(finished: &Vec<Vec<i32>>, size: &usize) {
    let mut img: RgbImage = ImageBuffer::new(u32::try_from(*size).unwrap(), u32::try_from(*size).unwrap());
    for (pos_x, row) in finished.iter().enumerate() {
        for (pos_y, value) in row.iter().enumerate() {
            img.put_pixel(u32::try_from(pos_x).unwrap(), u32::try_from(pos_y).unwrap(), to_color(value))
        }
    }
    img.save("test.png").unwrap();
}

fn to_color(value: &i32) -> image::Rgb<u8> {
    match *value {
        0 => return image::Rgb([54, 22, 184]),
        1 => return image::Rgb([207, 112, 52]),
        2 => return image::Rgb([65, 113, 159]),
        3 => return image::Rgb([126, 0, 0]),
        _ => return image::Rgb([0, 0, 0])
    }
}